<%@include file="/libs/foundation/global.jsp"%>
<cq:includeClientLib categories="xtype" />
 
<%@ page import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.foundation.Image,
                    com.day.cq.wcm.api.components.DropTarget,
                    com.day.cq.wcm.api.components.EditConfig,
                    com.day.cq.wcm.commons.WCMUtils,
                    org.apache.jackrabbit.commons.JcrUtils"
                        %>
 
----------------------------------
 
<b>*** mytype component starts ***</b>
 
 
 
 
<b>Here are the values that the user entered into custom xtype controls:</b>
 
<% Node node = null;
     
//This app logic gets back all values the user entered into the custom xtype and
//writes out the values to the CQ web page - the number of fields is unknown since
//each custom xtype is located on a multi-field
if(resource.adaptTo(Node.class)!=null)
    {
 
    node=resource.adaptTo(Node.class);
 
    PropertyIterator props=null;
    if (node.getProperties()!=null)
        props = node.getProperties();
 
        while (props.hasNext()) {
            Property prop = props.nextProperty();
            String name = prop.getName();
            //System.out.println("property -- "+name);
            String value=null;
            String[] linkFields =null;
 
                if (prop.getDefinition().isMultiple() && (name.equalsIgnoreCase("multi"))) {
                    StringBuffer buf = new StringBuffer();
                    
                    //Get the values entered into the custom xtype values
                    Value[] values = prop.getValues();
                    for (int i = 0; i < values.length; i++) {
                        buf.append(i > 0 ? "," : "");
                        buf.append(values[i].getString());
                    }
 
                    value = buf.toString();
                    String[] tokens = value.split(",");
 
                    for (int i=0;i<tokens.length;i++) 
                        {
                            //System.out.println(" tokens "+tokens[i]);
                            linkFields = tokens[i].split("\\\\");
                            for (int k=0; k<linkFields.length; k++)
                                {
                                //System.out.println(" value of k "+ k + "    linkfield"  + linkFields[k] );
                                if (k==0){
                                    %> <b>Type:</b> <%= linkFields[k] %>,<% continue;}
                                if (k==1){
                                    %> <b>Text: </b><%= linkFields[k] %>,<%continue;}
                                if (k==2){
                                    %> <b>URL: </b><%= linkFields[k] %><%continue;}
                                }
                        }
     
                }
                else {
                    if (name.equalsIgnoreCase("multi")){
                value = prop.getString();
                    linkFields = value.split("\\\\");
                            for (int k=0; k<linkFields.length; k++)
                                {
                                //System.out.println(" value of k "+ k + "    linkfield"  + linkFields[k] );
                                if (k==0){
                                    %> <b>Type:</b> <%= linkFields[k] %>,<% continue;}
                                if (k==1){
                                    %> <b>Text: </b><%= linkFields[k] %>,<%continue;}
                                if (k==2){
                                    %> <b>URL: </b><%= linkFields[k] %><%continue;}
                                }
                            }
                    }
                }
        }
        %>
<b>**myxtype component ends**</b>
-------------------------------